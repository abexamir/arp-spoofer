

# arp_spoof.py
* Python3 ARP Spoofer.
* Libraries used: scapy, time.
* Must be run as root.


# Usage
```bash
python3 arp_spoof.py -t [target ip address] -g [router ip address]
```

